This is the readme for the cnc cutter in the makerspace. Current documentation is primarily related to PCB milling. 

## Milling of single-sided PCB 
Here are the parameters for drilling, tracing and cutout that has been working well in Flatcam 8.5.

### Cutout parameters
Mill bit diameter: 1.5mm C-bit  

X/Y feed rate: 60  

Cut depth: 2.3mm  

Multi depth: 0.575mm (This will result in 4 passes before going through the PCB.)  


### Tracing parameters
Mill bit diameter: 0.5mm C-bit  

X/Y feed rate: 200  

Cut depth: 0.1mm  

Multi depth: 0.05mm (Two passes before cutting through the copper, so the bit won't break.)  


Trace passes: 3  

Trace overlap: 40% (Lower the this will leave thin copper lines on the glass fiber.)  


### Drill parameters  

Drill bit diameter: 1mm   

Z(plunge rate) feed rate: 60  

Cut depth: 2mm  


